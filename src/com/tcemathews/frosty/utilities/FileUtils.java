package com.tcemathews.frosty.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import android.webkit.MimeTypeMap;

public class FileUtils {
	static public String getMimeType(String url) {
	    String type = null;
	    String extension = MimeTypeMap.getFileExtensionFromUrl(url);
	    if (extension != null) {
	        MimeTypeMap mime = MimeTypeMap.getSingleton();
	        type = mime.getMimeTypeFromExtension(extension);
	    }
	    return type;
	}
	
	static public String getExtension(String url) {
		return MimeTypeMap.getFileExtensionFromUrl(url);
	}
	
	static public boolean isChildOf(File testChild, File testParent) throws Exception {
		if ( testParent == null ) {
			throw new Exception("The testParent must not be null.");
		}
		if ( testChild == null ) {
			throw new Exception("The testChild must not be null.");
		}
		
		File parent = testChild.getParentFile();
		while ( parent != null ) {
			if ( parent.equals(testParent)) {
				return true;
			}
			parent = parent.getParentFile();
		}
		return false;
	}
	
	static public boolean canCopy(File source, File destination) {
		if ( source.equals(destination) ) {
			return false;
		}
		
		try {
			if ( isChildOf(destination, source) ) {
				return false;
			}
		}
		catch ( Exception e ) {
			return false;
		}
		
		return true;
	}
	
	static public void copy(File source, File destination) throws Exception {
		if ( source.exists() == false ) {
			throw new Exception("Source does not exist.");
		}
		if ( canCopy(source, destination) == false ) {
			throw new Exception("Can't copy to this destination.");
		}
		
		if ( source.isDirectory() ) {
			// create the directory in the destination
			if ( destination.exists() == false ) {
				destination.mkdirs();
			}
			// recursively create the files from the old directory into the new directory
			File[] files = source.listFiles();
			for ( int i = 0; i < files.length; i++ ) {
				File f = files[i];
				File nf = new File(destination.getAbsolutePath() + File.separator + f.getName());
				try {
					copy(f, nf);
				}
				catch ( Exception e ) {
					System.out.println("E! " + e.getMessage());
				}
			}
			return;
		}
		
		InputStream istream = new FileInputStream(source);
		OutputStream ostream = new FileOutputStream(destination);
		
		byte[] buffer = new byte[1024];
		int len;
		while ( (len = istream.read(buffer)) > 0 ) {
			ostream.write(buffer, 0, len);
		}
		istream.close();
		ostream.close();
	}
	
	static public void delete(File source) throws Exception {
		if ( source.isDirectory() ) {
			File[] files = source.listFiles();
			for ( int i = 0; i < files.length; i++ ) {
				try {
					delete(files[i]);
				}
				catch ( Exception e ) {
					// do nothing
				}
			}
		}
		
		if ( source.delete() == false ) {
			throw new Exception("Could not delete file " + source.getPath());
		}
	}
}
