package com.tcemathews.frosty.widget;

import android.content.Context;
import android.view.KeyEvent;
import android.widget.EditText;

public class NoBreakEditText extends EditText {
	public NoBreakEditText(Context context) {
		super(context);
	}
	
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ( keyCode == KeyEvent.KEYCODE_ENTER ) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
    }
}
