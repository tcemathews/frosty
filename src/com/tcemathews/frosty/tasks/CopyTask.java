package com.tcemathews.frosty.tasks;

import java.io.File;

import com.tcemathews.frosty.R;
import com.tcemathews.frosty.core.App;
import com.tcemathews.frosty.core.Listing;
import com.tcemathews.frosty.utilities.FileUtils;

import android.app.ProgressDialog;
import android.os.AsyncTask;

public class CopyTask extends AsyncTask<Listing, Integer, Void> {
	protected App _app;
	protected File _destination;
	protected ProgressDialog _progressDialog;
	
	public CopyTask(File destination, App app) {
		_destination = destination;
		_app = app;
	}
	
	@Override
	protected void onPreExecute() {
		_progressDialog = new ProgressDialog(_app);
		_progressDialog.setTitle(_app.getString(R.string.msg_pasting));
		_progressDialog.setCancelable(false);
		_progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		_progressDialog.show();
	}
	
	@Override
	protected void onPostExecute(Void result) {
		_progressDialog.dismiss();
		System.out.println("finished");
		_app.getOperator().refresh();
		_app.getListings().refresh();
	}
	
	@Override
	protected void onProgressUpdate(Integer... progress) {
		_progressDialog.setProgress(progress[0]);
		_progressDialog.setMax(progress[1]);
	}
	
	@Override
	protected Void doInBackground(Listing... listings) {
		
		
		Listing[] items = listings;
		for ( int i = 0; i < items.length; i++ ) {
			File f = items[i].getFile();
			File nf = new File(_destination.getAbsolutePath() + File.separator + f.getName());
			if ( FileUtils.canCopy(f, nf) ) {
				try {
					FileUtils.copy(f, nf);
				}
				catch ( Exception e ) {
					System.out.println(e.getMessage());
				}
			}
			else {
				System.out.println("Cant copy files: " + f.getPath() + ", " + nf.getPath());
			}
			publishProgress(i+1, items.length);
		}
		
		return null;
	}
}
