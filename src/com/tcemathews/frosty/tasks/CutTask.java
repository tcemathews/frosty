package com.tcemathews.frosty.tasks;

import java.io.File;

import com.tcemathews.frosty.core.App;
import com.tcemathews.frosty.core.Listing;
import com.tcemathews.frosty.utilities.FileUtils;

public class CutTask extends CopyTask {

	public CutTask(File destination, App app) {
		super(destination, app);
	}
	
	@Override
	protected Void doInBackground(Listing... listings) {
		Listing[] items = listings;
		
		for ( int i = 0; i < items.length; i++ ) {
			File f = items[i].getFile();
			File nf = new File(_destination.getAbsolutePath() + File.separator + f.getName());
			if ( FileUtils.canCopy(f, nf) ) {
				try {
					FileUtils.copy(f, nf);
					FileUtils.delete(items[i].getFile());
				}
				catch ( Exception e ) {
					System.out.println(e.getMessage());
				}
			}
			else {
				System.out.println("Cant copy files: " + f.getPath() + ", " + nf.getPath());
			}
			publishProgress(i+1, items.length);
		}
		
		return null;
	}
}
