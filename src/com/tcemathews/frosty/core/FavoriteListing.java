package com.tcemathews.frosty.core;

import java.io.File;

public class FavoriteListing extends Listing {
	private String _nickname;
	private boolean _removable;
	
	public FavoriteListing(File file, String nickname, boolean removable) {
		super(file);
		_nickname = nickname;
		_removable = removable;
	}
	
	@Override
	public String toString() {
		return _nickname;
	}
	
	public void setNickname(String value) {
		_nickname = value;
	}
	
	public boolean getRemovable() {
		return _removable;
	}
}
