package com.tcemathews.frosty.core;

import java.io.File;
import java.util.ArrayList;

import com.tcemathews.frosty.R;
import com.tcemathews.frosty.components.ActionBarComponent;
import com.tcemathews.frosty.components.ListingsComponent;
import com.tcemathews.frosty.components.OperatorComponent;
import com.tcemathews.frosty.tasks.CopyTask;
import com.tcemathews.frosty.tasks.CutTask;
import com.tcemathews.frosty.utilities.FileUtils;
import com.tcemathews.frosty.utilities.IntentUtils;
import com.tcemathews.frosty.widget.NoBreakEditText;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class App extends Activity {
	private boolean _inited;
	private boolean _initedMenu;
	private OperatorComponent _operator;
	private ActionBarComponent _actionbar;
	private ListingsComponent _listings;
	
	public App() {
		_inited = false;
		_initedMenu = false;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		init();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		initMenu(menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch ( item.getItemId() ) {
		case android.R.id.home:
			displayMainListings();
			return true;
		case com.tcemathews.frosty.R.id.menu_settings:
			displaySettings();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent e) {
		// if we hit the back button lets go up a directory, if we can't go up close the application.
		if ( keyCode == KeyEvent.KEYCODE_BACK ) {
			if ( _operator.canRetreatListing() ) {
				_operator.retreatListing();
				_listings.refresh();
			}
			else {
				displayConfirmQuit();
			}
			return true;
		}
		
		return super.onKeyDown(keyCode, e);
	}
	
	public OperatorComponent getOperator() {
		return _operator;
	}
	
	public ActionBarComponent getActionbar() {
		return _actionbar;
	}
	
	public ListingsComponent getListings() {
		return _listings;
	}
	
	private void init() {
		if ( _inited ) return;
		_inited = true;
		// settings
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		// the operator
		_operator = new OperatorComponent(this);
		// the action bar
		_actionbar = new ActionBarComponent(this, getActionBar());
		// the main list view
		ListView listView = (ListView)findViewById(R.id.list);
		if ( listView != null) _listings = new ListingsComponent(this, listView);
	}
	
	private void initMenu(Menu menu) {
		if ( _initedMenu ) return;
		_initedMenu = true;
		getMenuInflater().inflate(R.menu.activity_main, menu);
		_actionbar.setMenu(menu);
		displayMainListings();
	}
	
	public void displaySettings() {
		startActivity(new Intent(this, Settings.class));
	}
	
	public void displaySimpleAlert(String title, String message, String buttonLabel) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setNeutralButton(buttonLabel, null);
		builder.show();
	}
	
	public void displaySimpleToast(CharSequence text, int length) {
		Toast.makeText(this, text, length).show();
	}
	
	public void displayConfirmQuit() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.choices_quit));
		builder.setMessage(getString(R.string.msg_confirm_quit));
		builder.setPositiveButton(getString(R.string.action_confirm), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				startActivity(intent);
			}
		});
		builder.setNegativeButton(getString(R.string.action_cancel), null);
		builder.show();
	}
	
	public void displayMainListings() {
		_operator.openMainListing();
		_listings.refresh();
	}
	
	public void displayListingsFromHistory(int position) {
		_operator.openHistoryPosition(position);
		_listings.refresh();
	}
	
	public void promptListing(Listing listing) {
		File file = listing.getFile();
		
		if ( !file.canRead() ) {
			displaySimpleAlert(getString(R.string.error_access), getString(R.string.msg_error_access), getString(R.string.action_okay));
			return;
		}
		
		if ( file.isDirectory() ) {
			_operator.openListingFromFile(file);
			_listings.refresh();
		}
		else if ( file.isFile() ) {
			execute(file, true);
		}
	}
	
	public void promptListingOptions(Listing listing) {
		String[] choices = getListingChoices(listing);
		if ( choices == null || choices.length == 0 ) {
			displaySimpleAlert(getString(R.string.error_no_options), getString(R.string.msg_error_no_options), getString(R.string.action_okay));
			return;
		}
		
		final Listing selectedListing = listing;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setItems(choices, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String choice = (String)((AlertDialog)dialog).getListView().getItemAtPosition(which);
				chooseListingOption(selectedListing, choice);
			}
		});
		builder.show();
	}

	private void promptAddFavorite(Listing listing) {
		final Listing selectedListing = listing;
		final NoBreakEditText input = new NoBreakEditText(this);
		input.setText(selectedListing.toString(), TextView.BufferType.EDITABLE);
		input.setSelection(input.getText().length());
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.choices_add_favorite));
		builder.setMessage(getString(R.string.msg_name_favorite));
		builder.setView(input);
		builder.setPositiveButton(getString(R.string.action_confirm), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String value = input.getText().toString();
				if ( value.isEmpty() ) {
					return;
				}
				
				_operator.addFavorite(selectedListing, value);
				_operator.refresh();
				_listings.refresh();
				displaySimpleToast("\"" + value + "\" " + getString(R.string.msg_was_added_favorites), Toast.LENGTH_SHORT);
			}
		});
		builder.setNegativeButton(getString(R.string.action_cancel), null);
		builder.show();
	}
	
	private void promptRemoveFavorite(FavoriteListing listing) {
		final FavoriteListing selectedListing = listing;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.choices_remove_favorite));
		builder.setMessage(getString(R.string.msg_confirm_remove_favorite) + "\n\n" + selectedListing.toString());
		builder.setPositiveButton(getString(R.string.action_confirm), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				_operator.removeFavorite(selectedListing);
				_operator.refresh();
				_listings.refresh();
				displaySimpleToast("\"" + selectedListing.toString() + "\" " + getString(R.string.msg_was_removed_favorite), Toast.LENGTH_SHORT);
			}
		});
		builder.setNegativeButton(getString(R.string.action_cancel), null);
		builder.show();
	}
	
	public void promptCreateDirectory() {
		final NoBreakEditText input = new NoBreakEditText(this);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.menu_new));
		builder.setMessage(getString(R.string.msg_name_directory));
		builder.setView(input);
		builder.setPositiveButton(getString(R.string.action_confirm), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String value = input.getText().toString();
				if ( value.isEmpty() ) {
					return;
				}
				
				Listing listing = _operator.getCurrentListing();
				if ( listing == null ) {
					displaySimpleAlert(getString(R.string.error_unknown), getString(R.string.msg_error_unknown), getString(R.string.action_okay));
					return;
				}
				
				File newDirectory = new File(listing.getFile().getAbsolutePath() + File.separator + value);
				if ( !newDirectory.mkdir() ) {
					displaySimpleAlert(getString(R.string.error_create_directory), getString(R.string.msg_error_create_directory), getString(R.string.action_okay));
				}
				else {
					_operator.refresh();
					_listings.refresh();
					displaySimpleToast("\"" + newDirectory.getName() + "\" " + getString(R.string.msg_was_created), Toast.LENGTH_SHORT);
				}
			}
		});
		builder.setNegativeButton(getString(R.string.action_cancel), null);
		builder.show();
	}
	
	public void confirmPaste() {
		Clipboard clipboard = _operator.getClipboard();
		_operator.setClipboard(null);
		_actionbar.displayPasteOption(false);
		
		if ( clipboard.getMode() == Clipboard.MODE_CUT ) {
			CutTask task = new CutTask(_operator.getCurrentListing().getFile(), this);
			task.execute(clipboard.getItems());
		}
		else {
			CopyTask task = new CopyTask(_operator.getCurrentListing().getFile(), this);
			task.execute(clipboard.getItems());
		}
	}
	
	public void cancelPaste() {
		_operator.setClipboard(null);
		_actionbar.displayPasteOption(false);
	}

	private String[] getListingChoices(Listing listing) {
		File file = listing.getFile();
		ArrayList<String> choices = new ArrayList<String>();
		
		if ( listing instanceof FavoriteListing ) {
			FavoriteListing fav = (FavoriteListing)listing;
			if ( fav.getRemovable() == true ) {
				choices.add(getString(R.string.choices_remove_favorite));
				choices.add(getString(R.string.choices_rename));
			}
		}
		else {
			if ( file.canRead() ) {
				if ( file.isDirectory() ) {
					choices.add(getString(R.string.choices_add_favorite));
				}
				if ( file.isFile() ) {
					choices.add(getString(R.string.choices_open_with));
				}
				choices.add(getString(R.string.choices_copy));
				choices.add(getString(R.string.choices_cut));
				choices.add(getString(R.string.choices_rename));
				choices.add(getString(R.string.choices_delete));
			}
		}
		
		return choices.toArray(new String[0]);
	}

	private void chooseListingOption(Listing listing, String choice) {
		if ( choice == getString(R.string.choices_remove_favorite) && listing instanceof FavoriteListing ) {
			promptRemoveFavorite((FavoriteListing)listing);
		}
		else if ( choice == getString(R.string.choices_add_favorite) ) {
			promptAddFavorite(listing);
		}
		else if ( choice == getString(R.string.choices_open_with) ) {
			execute(listing.getFile(), false);
		}
		else if ( choice == getString(R.string.choices_delete) ) {
			delete(listing.getFile());
		}
		else if ( choice == getString(R.string.choices_rename) ) {
			if ( listing instanceof FavoriteListing ) {
				renameFavorite((FavoriteListing)listing);
			}
			else {
				rename(listing.getFile());
			}
		}
		else if ( choice == getString(R.string.choices_copy) ) {
			copy(new Listing[]{listing});
		}
		else if ( choice == getString(R.string.choices_cut) ) {
			cut(new Listing[]{listing});
		}
	}

	private void cut(Listing[] listings) {
		_operator.setClipboard(new Clipboard(Clipboard.MODE_CUT, listings));
		_actionbar.displayPasteOption(true);
	}

	private void copy(Listing[] listings) {
		_operator.setClipboard(new Clipboard(Clipboard.MODE_COPY, listings));
		_actionbar.displayPasteOption(true);
	}

	private void execute(File file, boolean automatic) {
		Intent intent = new Intent();
		intent.setAction(android.content.Intent.ACTION_VIEW);
		String mimeType = FileUtils.getMimeType(file.getName());
		
		if ( mimeType != null ) {
			intent.setDataAndType(Uri.fromFile(file), mimeType);
		}
		else {
			intent.setData(Uri.fromFile(file));
		}
		
		if ( IntentUtils.isIntentAvailable(this, intent) ) {
			if ( automatic ) {
				startActivity(intent);
			}
			else {
				startActivity(Intent.createChooser(intent, getString(R.string.choices_open_with)));
			}
		}
		else {
			displaySimpleAlert(getString(R.string.error_open), getString(R.string.msg_error_open_file), getString(R.string.action_okay));
		}
	}
	
	private void delete(File file) {
		final File deleteFile = file;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.choices_delete));
		builder.setMessage(getString(R.string.msg_confirm_delete) + "\n\n" + deleteFile.getName());
		builder.setPositiveButton(getString(R.string.action_confirm), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String name = deleteFile.getName();
				try {
					FileUtils.delete(deleteFile);
					_operator.refresh();
					_listings.refresh();
					displaySimpleToast("\"" + name + "\" " + getString(R.string.msg_was_deleted), Toast.LENGTH_SHORT);
				}
				catch ( Exception e ) {
					displaySimpleAlert(getString(R.string.error_delete), getString(R.string.msg_error_delete), getString(R.string.action_okay));
				}
			}
		});
		builder.setNegativeButton(getString(R.string.action_cancel), null);
		builder.show();
	}
	
	private void rename(File file) {
		final File renameFile = file;
		final NoBreakEditText input = new NoBreakEditText(this);
		input.setText(renameFile.getName(), TextView.BufferType.EDITABLE);
		input.setSelection(input.getText().length());
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.choices_rename));
		builder.setView(input);
		builder.setPositiveButton(getString(R.string.action_confirm), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String value = input.getText().toString();
				
				if ( value.equals(renameFile.getName()) ) {
					return;
				}
				
				String newPathStr = renameFile.getParentFile().getPath();
				File newPath = new File(newPathStr + File.separator + value);
				if ( !renameFile.renameTo(newPath) ) {
					displaySimpleAlert(getString(R.string.error_rename), getString(R.string.msg_error_rename), getString(R.string.action_okay));
				}
				else {
					_operator.refresh();
					_listings.refresh();
					displaySimpleToast(getString(R.string.msg_renamed), Toast.LENGTH_SHORT);
				}
			}
		});
		builder.setNegativeButton(getString(R.string.action_cancel), null);
		builder.show();
	}
	
	private void renameFavorite(FavoriteListing listing) {
		final FavoriteListing selectedListing = listing;
		final NoBreakEditText input = new NoBreakEditText(this);
		input.setText(selectedListing.toString(), TextView.BufferType.EDITABLE);
		input.setSelection(input.getText().length());
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.choices_rename));
		builder.setMessage(getString(R.string.msg_name_favorite));
		builder.setView(input);
		builder.setPositiveButton(getString(R.string.action_confirm), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String oldName = selectedListing.toString();
				String value = input.getText().toString();
				if ( value.isEmpty() && oldName.contentEquals(value) ) {
					return;
				}
				
				_operator.renameFavorite(selectedListing, value);
				_operator.refresh();
				_listings.refresh();
				displaySimpleToast("\"" + oldName + "\" " + getString(R.string.msg_was_renamed_to) + " \"" + value + "\"", Toast.LENGTH_SHORT);
			}
		});
		builder.setNegativeButton(getString(R.string.action_cancel), null);
		builder.show();
	}
}
