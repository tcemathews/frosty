package com.tcemathews.frosty.core;

import java.io.File;

public class Listing {
	protected File _file;
	
	private String _name;
	
	public int sortIndex;
	
	public Listing(File file) {
		_file = file;
		_name = file.getName();
		if ( _name.contentEquals("") ) {
			_name = "/";
		}
		sortIndex = -1;
	}
	
	@Override
	public String toString() {
		return _name;
	}
	
	public File getFile() {
		return _file;
	}
}
