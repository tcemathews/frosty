package com.tcemathews.frosty.core;

public class Clipboard {
	public static int MODE_COPY = 0;
	public static int MODE_CUT = 1;
	
	private int _mode;
	private Listing[] _items;
	
	public Clipboard(int mode, Listing[] items) {
		_mode = mode;
		if ( _mode != MODE_CUT ) {
			_mode = MODE_COPY;
		}
		
		_items = items;
	}
	
	public int getMode() {
		return _mode;
	}
	
	public Listing[] getItems() {
		return _items;
	}
	
	public boolean hasItems() {
		return _items.length > 0;
	}
}
