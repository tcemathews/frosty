package com.tcemathews.frosty.components;

import com.tcemathews.frosty.core.App;
import com.tcemathews.frosty.core.Component;
import com.tcemathews.frosty.core.Listing;

import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.SearchView;

public class ActionBarComponent extends Component implements OnNavigationListener {
	private ActionBar _actionbar;
	private Menu _menu;
	private MenuItem _createOption;
	private MenuItem _pasteOption;
	private SearchView _searchView;
	private boolean _blockUpdate;
	
	public ActionBarComponent(App application, ActionBar actionbar) {
		super(application);
		_actionbar = actionbar;
		_actionbar.setHomeButtonEnabled(true);
		_actionbar.setTitle("Frosty Explorer");
		_actionbar.setSubtitle("Favorites");
		_blockUpdate = false;
	}
	
	public ActionBar getActionBar() {
		return _actionbar;
	}
	
	public void setMenu(Menu menu) {
		_menu = menu;
		
		// do the new folder
		_createOption = (MenuItem)_menu.findItem(com.tcemathews.frosty.R.id.menu_new);
		_createOption.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				_app.promptCreateDirectory();
				return true;
			}
		});
		
		// do the paste options
		_pasteOption = (MenuItem)_menu.findItem(com.tcemathews.frosty.R.id.menu_paste);
		_menu.findItem(com.tcemathews.frosty.R.id.menu_paste_cancel).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				_app.cancelPaste();
				return true;
			}
		});
		_menu.findItem(com.tcemathews.frosty.R.id.menu_paste_confirm).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				_app.confirmPaste();
				return true;
			}
		});
		
		// do the search view
		_searchView = (SearchView)_menu.findItem(com.tcemathews.frosty.R.id.menu_search).getActionView();
		_searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextChange(String newText) {
				_app.getListings().filter(newText);
				return false;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				_app.getListings().filter(query);
				return true;
			}
		});
		_searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if ( !hasFocus ) {
					_app.getListings().clearFilter();
				}
			}
		});
	}
	
	public void closeSearch() {
		_menu.findItem(com.tcemathews.frosty.R.id.menu_search).collapseActionView();
	}
	
	public void displayHistory(boolean value) {
		if ( value && _actionbar.getNavigationMode() != ActionBar.NAVIGATION_MODE_LIST ) {
			_actionbar.setDisplayShowTitleEnabled(false);
			_actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		}
		else if ( !value && _actionbar.getNavigationMode() != ActionBar.NAVIGATION_MODE_STANDARD ){
			_actionbar.setDisplayShowTitleEnabled(true);
			_actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		}
	}
	
	public void displayCreateDirectoryOption(boolean value) {
		_createOption.setEnabled(value);
		_createOption.setVisible(value);
	}
	
	public void displayPasteOption(boolean value) {
		_pasteOption.setEnabled(value);
		_pasteOption.setVisible(value);
	}

	public void refresh() {
		boolean can = _app.getOperator().canRetreatListing();
		displayHistory(can);
		displayCreateDirectoryOption(can);
		
		if ( can && _blockUpdate == false ) {
			Listing[] history = _app.getOperator().getHistory().toArray(new Listing[0]);
			ArrayAdapter<Listing> adapter = new ArrayAdapter<Listing>(
					_actionbar.getThemedContext(), 
					android.R.layout.simple_list_item_1, 
					android.R.id.text1, 
					history);
			_actionbar.setListNavigationCallbacks(adapter, this);
			_actionbar.setSelectedNavigationItem(history.length - 1);
		}
		_blockUpdate = false;
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		_blockUpdate = true;
		_app.displayListingsFromHistory(itemPosition);
		return true;
	}
}
