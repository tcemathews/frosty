package com.tcemathews.frosty.components;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

import com.tcemathews.frosty.R;
import com.tcemathews.frosty.core.App;
import com.tcemathews.frosty.core.Clipboard;
import com.tcemathews.frosty.core.Component;
import com.tcemathews.frosty.core.FavoriteListing;
import com.tcemathews.frosty.core.Listing;
import com.tcemathews.frosty.utilities.FileUtils;

public class OperatorComponent extends Component {
	private ArrayList<Listing> _history;
	private ArrayList<FavoriteListing> _favorites;
	private Listing[] _listings;
	private ListingFileFilter _filter;
	private Clipboard _clipboard;
	
	public OperatorComponent(App app) {
		super(app);
		_clipboard = null;
		_history = new ArrayList<Listing>();
		_favorites = new ArrayList<FavoriteListing>();
		_listings = new Listing[]{};
		_filter = new ListingFileFilter();
		
		// set up the preferences
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(_app);
		SharedPreferences.OnSharedPreferenceChangeListener listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
			@Override
			public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
				if ( key.equals(_app.getString(R.string.pref_show_hidden_files)) ) {
					_filter.showHiddenFiles = sharedPreferences.getBoolean(key, false);
					refresh();
				}
			}
		};
		preferences.registerOnSharedPreferenceChangeListener(listener);
		_filter.showHiddenFiles = preferences.getBoolean(_app.getString(R.string.pref_show_hidden_files), false);
		
		// set up default favorites
		FavoriteListing explore = new FavoriteListing(new File("/"), "Explore", false);
		FavoriteListing external = new FavoriteListing(Environment.getExternalStorageDirectory(), "External Storage", false);
		explore.sortIndex = 1;
		external.sortIndex = 2;
		_favorites.add(explore);
		_favorites.add(external);
		
		try {
			DataInputStream istream = new DataInputStream(_app.openFileInput("favorites"));
			try {
				for(;;) {
					String name = istream.readUTF();
					String path = istream.readUTF();
					_favorites.add(new FavoriteListing(new File(path), name, true));
				}
			}
			catch ( EOFException e ) {
				// do nothing...
			}
			istream.close();
		} catch ( IOException e ) {
			// do nothing...
		}
	}
	
	public Clipboard getClipboard() {
		return _clipboard;
	}
	
	public void setClipboard(Clipboard value) {
		if ( value != null && value.hasItems() ) {
			_clipboard = value;
			return;
		}
		_clipboard = null;
	}
	
	public boolean canPaste() {
		return _clipboard != null && _clipboard.hasItems();
	}
	
	public Listing[] getListings() {
		return _listings;
	}
	
	public ArrayList<Listing> getHistory() {
		return _history;
	}
	
	public Listing getCurrentListing() {
		if ( _history.isEmpty() ) {
			return null;
		}
		return _history.get(_history.size() - 1);
	}
	
	public void openMainListing() {
		_history.clear();
		_listings = _favorites.toArray(new Listing[0]);
		_app.getActionbar().refresh();
	}
	
	public boolean canRetreatListing() {
		return _history.size() > 0;
	}
	
	public void retreatListing() {
		if ( _history.size() == 1 ) {
			openMainListing();
		}
		else {
			openParentListing();
		}
	}
	
	public void openParentListing() {
		if ( _history.isEmpty() ) {
			return;
		}
		
		_history.remove(_history.size() - 1);
		refresh();
	}
	
	public void openHistoryPosition(int position) {
		int count = _history.size();
		
		if ( position < 0 || position >= count ) {
			return;
		}
		
		for ( int i = count - 1; i > position; i-- ) {
			_history.remove(i);
		}
		
		refresh();
	}

	public boolean openListingFromFile(File file) {
		if ( !file.isDirectory() ) {
			return false;
		}
		
		Listing listing = new Listing(file);
		_history.add(listing);
		refresh();
		return true;
	}
	
	public void refresh() {
		// if we have no history we are showing the favorites menu
		if ( _history.isEmpty() ) {
			openMainListing();
			return;
		}
		
		// if we have a history show the last directory we are in
		Listing currentListing = _history.get(_history.size() - 1);
		File currentFile = currentListing.getFile();
		File[] files = currentFile.listFiles(_filter);
		int count = files.length;
		Listing[] listings = new Listing[count];
		for ( int i = 0; i < count; i++ ) {
			listings[i] = new Listing(files[i]);
		}
		_listings = listings;
		_app.getActionbar().refresh();
	}
	
	public boolean hasFavorite(Listing listing) {
		int count = _favorites.size();
		for ( int i = 0; i < count; i++ ) {
			if ( _favorites.get(i).getFile().equals(listing.getFile()) ) {
				return true;
			}
		}
		return false;
	}
	
	public void addFavorite(Listing listing, String name) {
		if ( hasFavorite(listing) == false ) {
			FavoriteListing fav = new FavoriteListing(listing.getFile(), name, true);
			_favorites.add(fav);
			writeFavorites();
		}
	}
	
	public void removeFavorite(FavoriteListing listing) {
		_favorites.remove(listing);
		writeFavorites();
	}

	public void renameFavorite(FavoriteListing listing, String value) {
		listing.setNickname(value);
		writeFavorites();
	}
	
	private void writeFavorites() {
		try {
			DataOutputStream ostream = new DataOutputStream(_app.openFileOutput("favorites", Context.MODE_PRIVATE));
			int count = _favorites.size();
			for ( int i = 0; i < count; i++ ) {
				FavoriteListing fav = _favorites.get(i);
				if ( fav.getRemovable() == true ) {
					ostream.writeUTF(fav.toString());
					ostream.writeUTF(fav.getFile().getAbsolutePath());
				}
			}
			ostream.close();
		} catch ( IOException e ) {
			System.out.println("Data Exception in writing favorites!");
		}
	}
	
	private class ListingFileFilter implements FileFilter {
		public boolean showHiddenFiles;
		public ArrayList<String> extensionFilters;
		
		public ListingFileFilter() {
			showHiddenFiles = false;
			extensionFilters = new ArrayList<String>();
		}
		
		@Override
		public boolean accept(File file) {
			if ( showHiddenFiles == false && file.isHidden() ) {
				return false;
			}
			
			if ( extensionFilters.contains(FileUtils.getExtension(file.getName())) ) {
				return false;
			}
			
			return true;
		}
	}
}
