package com.tcemathews.frosty.components;

import java.util.Comparator;
import java.util.Locale;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.tcemathews.frosty.core.App;
import com.tcemathews.frosty.core.Component;
import com.tcemathews.frosty.core.Listing;

public class ListingsComponent extends Component {
	private ArrayAdapter<Listing> _adapter;
	private ListView _listView;
	
	public ListingsComponent(App app, ListView listView) {
		super(app);
		_listView = listView;
		_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
				_app.getActionbar().closeSearch();
				_app.promptListing(_adapter.getItem(position));
			}
		});
		_listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
				_app.promptListingOptions(_adapter.getItem(position));
				return true;
			}
		});
		refresh();
	}
	
	public void refresh() {
		displayListings(_app.getOperator().getListings());
	}
	
	public void displayListings(Listing[] listings) {
		_adapter = new ArrayAdapter<Listing>(_app, android.R.layout.simple_list_item_1, listings);
		_adapter.sort(new ListingComparator());
		_listView.setAdapter(_adapter);
	}
	
	public void filter(String value) {
		_adapter.getFilter().filter(value);
	}
	
	public void clearFilter() {
		_adapter.getFilter().filter(null);
	}


	private class ListingComparator implements Comparator<Listing> {
		@Override
		public int compare(Listing lhs, Listing rhs) {
			// custom sort
			int si = compareSortIndex(lhs, rhs);
			if ( si != 0 ) {
				return si;
			}
			
			// sort by folder then file
			if ( lhs.getFile().isDirectory() && rhs.getFile().isFile() ) {
				return -1;
			}
			else if ( lhs.getFile().isFile() && rhs.getFile().isDirectory() ) {
				return 1;
			}
			// just sort by name
			String na = lhs.toString().toLowerCase(Locale.getDefault());
			String nb = rhs.toString().toLowerCase(Locale.getDefault());
			return na.compareTo(nb);
		}
		
		public int compareSortIndex(Listing lhs, Listing rhs) {
			if ( lhs.sortIndex < 0 || rhs.sortIndex < 0 ) {
				return 0;
			}
			else if ( lhs.sortIndex < rhs.sortIndex ) {
				return -1;
			}
			else if ( lhs.sortIndex > rhs.sortIndex ) {
				return 1;
			}
			return 0;
		}
	}
}
